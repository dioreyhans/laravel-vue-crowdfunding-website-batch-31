<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware('email')->group(function(){
    Route::get('/route-1', [App\Http\Controllers\HomeController::class, 'email']);
});

Route::middleware(['email' , 'admin'])->group(function(){
    Route::get('/route-2', [App\Http\Controllers\HomeController::class, 'admin']);
});