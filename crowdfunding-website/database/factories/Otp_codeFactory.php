<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class Otp_codeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'otp' => mt_rand(1000000000, 9999999999),
        ];
    }
}
