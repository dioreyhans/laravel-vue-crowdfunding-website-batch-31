<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\otp_code;
use Illuminate\Support\Str;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\Role::create(['id'=> (string) Str::uuid(),'name' => 'admin']);
        //\App\Models\Role::create(['id'=> (string) Str::uuid(),'name' => 'user']);
        \App\Models\User::factory(3)->create();

    }
}
