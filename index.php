
<?php

abstract class Hewan {

    public $nama;
    public $darah=50;
    public $jumlahkaki;
    public $keahlian;
    abstract public function atraksi();
   }
   
   trait Fight {
   
       public $attackpower;
       public $defencepower;
       
       function serang($musuh){
        echo $this->nama." sedang menyerang ".$musuh->nama."<br><br>";
        $this->diserang($musuh);
        }
       function diserang($hewan)
       {
        echo $hewan->nama." sedang diserang ".$this->nama."<br><br>";
        $hewan->darah = $hewan->darah - $this->attackpower/$hewan->defencepower;
       }

      }
   
      class Elang extends Hewan{
        use Fight;
        public function __construct($nama) {
          $this->nama=$nama;
          $this->jumlahkaki=2;
          $this->keahlian="Terbang Tinggi";
          $this->attackpower=10;
          $this->defencepower=5;
        }
        public function atraksi() {
          echo $this->nama." sedang ".$this->keahlian;
        }
        
       function getInfoHewan()
       {
        echo "
        Nama : ".$this->nama. "<br>
        Darah : " . $this->darah."<br>
        Jumlah kaki : " . $this->jumlahkaki. "<br>
        Keahlian : " . $this->keahlian."<br>
        Jenis : Elang <br>
        Attack : " . $this->attackpower. "<br>
        Defence : " . $this->defencepower;
       }
      
      }
      class Harimau extends Hewan{
        use Fight;
        public function __construct($nama) {
          $this->nama=$nama;
          $this->jumlahkaki=4;
          $this->keahlian="Berlari Kencang";
          $this->attackpower=7;
          $this->defencepower=8;
        }
        public function atraksi() {
          echo $this->nama." sedang ".$this->keahlian;
        }
        
        function getInfoHewan()
        {
        echo "
        Nama : ".$this->nama. "<br>
        Darah : " . $this->darah."<br>
        Jumlah kaki : " . $this->jumlahkaki. "<br>
        Keahlian : " . $this->keahlian."<br>
        Jenis : Harimau <br>
        Attack : " . $this->attackpower. "<br>
        Defence : " . $this->defencepower;
        }
       
       }
 
       $elang = new Elang('elang_1');
       $harimau = new Harimau('harimau_1');
       echo $elang->getInfoHewan()."<br><br>";
       echo $harimau->getInfoHewan()."<br><br>";
       echo $elang->atraksi()."<br><br>";
       echo $harimau->atraksi()."<br><br>";
       echo $harimau->serang($elang);
       echo $elang->getInfoHewan()."<br><br>";
       echo $harimau->getInfoHewan()."<br><br>";
?>